#!/usr/bin/python3

'''
Copyright (c) 2017  Jonas Thiem et al.

This software is provided 'as-is', without any express or implied
warranty. In no event will the authors be held liable for any damages
arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute it
freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented; you must not
   claim that you wrote the original software. If you use this software
   in a product, an acknowledgment in the product documentation would be
   appreciated but is not required.
2. Altered source versions must be plainly marked as such, and must not be
   misrepresented as being the original software.
3. This notice may not be removed or altered from any source distribution.
'''

import argparse
import logging
import shlex
import subprocess
import sys


def get_logger(out):
    logger = logging.Logger(out.name)
    logger.setLevel(logging.INFO)
    handler = logging.StreamHandler(out)
    handler.setLevel(logging.DEBUG)
    formatter = logging.Formatter(
        '[%(filename)s/%(funcName)s] %(levelname)s: %(message)s')
    handler.setFormatter(formatter)
    logger.addHandler(handler)
    return logger


stdout = get_logger(sys.stdout)
stderr = get_logger(sys.stderr)


def run_cmd(cmd, throw_error=False):
    global ssh_arg
    if type(cmd) == str:
        cmd = shlex.split(cmd)

    def cmd_array_as_cmd(array):
        def convert_param(c):
            if c == "|" or c == "||" or\
                    c == "&&" or c == ">>":
                return c
            return shlex.quote(c)
        return str(" ".join([convert_param(c) for c in cmd]))
    stderr.debug(cmd_array_as_cmd(cmd))
    exit_code = 0
    output = ""
    try:
        output = subprocess.check_output(
            ["ssh"] + ssh_arg + ["--", cmd_array_as_cmd(cmd)],
            stderr=subprocess.STDOUT, shell=False)
    except subprocess.CalledProcessError as e:
        output = e.output
        exit_code = e.returncode
    try:
        output = output.decode("utf-8", "replace")
    except AttributeError:
        pass
    if exit_code != 0 and throw_error:
        raise RuntimeError(
            "failed to run cmd (exit_code != 0): " + str(cmd) +
            "\nfailure output:\n" + str(output))
    return exit_code, output


def run_cmds(cmds, throw_error=False):
    cmds = cmds.replace("\r\n", "\n").replace("\r", "\n").strip()
    while cmds.find("\n\n") >= 0:
        cmds = cmds.replace("\n\n", "\n")
    combined_output = ""
    for line in cmds.split("\n"):
        cmd = line.lstrip()
        if cmd.startswith("#"):
            continue
        exit_code, output = run_cmd(cmd, throw_error=False)
        if not combined_output.endswith("\n") and \
                not output.startswith("\n"):
            combined_output += "\n"
        combined_output += output
        if exit_code != 0:
            if throw_error:
                raise RuntimeError(
                    "failed to run cmd (exit_code != 0): " + str(cmd) +
                    "\nfailure output:\n" + str(output))
            return exit_code, combined_output
    return 0, combined_output


def uncommented_lines(lines):
    lines = lines.replace("\r\n", "\n").replace("\r", "\n").strip()
    while lines.find("\n\n") >= 0:
        lines = lines.replace("\n\n", "\n")
    result = []
    for line in lines.split("\n"):
        if line.lstrip().startswith("#"):
            continue
        result.append(line)
    return "\n".join(result)


def get_codename():
    result, output = run_cmd("lsb_release -c")
    output = output.rstrip().replace("\t", " ")
    output = output[output.rfind(" "):].strip()
    assert(len(output) > 0)
    return output


def get_swap():
    result, output = run_cmd('cat /proc/swaps | grep -v "^#" | wc -l')
    output = int(output)
    assert(output > 0)
    return output > 1


def curl_and_install_key(url):
    exit_code, output = run_cmd('curl --version')
    if exit_code != 0:
        # Curl is missing
        stdout.debug('Installing missing curl')
        run_cmd(
            'apt-get update --fix-missing && ' +
            'apt-get install -y curl',
            throw_error=True)
    return run_cmd(
        'curl -fsSL ' + url + ' | apt-key add -',
        throw_error=True)


def deploy_kubernetes():
    if get_swap():
        stderr.error(
            "Swap enabled, kubernetes will not start - aborting",
        )
        sys.exit(1)

    # 1.
    stdout.info('Install kubernetes key')

    result, output = curl_and_install_key(
        'curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg')

    # Add repository if needed
    result, file_output = run_cmds(
        '''cat /etc/apt/sources.list.d/kubernetes.list''')
    if result != 0 or \
            uncommented_lines(file_output).find("kubernetes") < 0:
        result, output = run_cmd(
            'echo "deb http://apt.kubernetes.io/ kubernetes-' +
            get_codename() +
            ' main" >> /etc/apt/sources.list.d/kubernetes.list',
            throw_error=True)

    # 2.
    stdout.info('Update package information')

    result, output = run_cmds(
        "apt-get update")
    if result != 0:
        raise RuntimeError(
            "deploy unexpectedly failed with exit_code != 0. " +
            "error command output: " + output)

    # 3.
    stdout.info('Install docker')
    result, output = run_cmds(
        "apt-get install -y docker-engine")
    if result != 0:
        raise RuntimeError(
            "deploy unexpectedly failed with exit_code != 0. " +
            "error command output: " + output)

    # 4.
    stdout.info('Install kubernetes')
    result, output = run_cmds(
     "apt-get install -y kubelet kubeadm kubectl kubernetes-cni")
    if result != 0:
        raise RuntimeError(
            "deploy unexpectedly failed with exit_code != 0. " +
            "error command output: " + output)
    stdout.info('success.')


def get_kubectl_cluster_info():
    result, output = run_cmd(
        '''bash -c "KUBECONFIG=/root/admin.conf kubectl cluster-info dump"''')
    if result != 0:
        if "The connection" in output and "was refused" in output:
            return None  # no cluster present.
        raise RuntimeError(
            "unexpected failure of cluster-info cmd " +
            "with exit code " + str(result) + ", output: " +
            str(output))
    return output


def kubernetes_cluster_is_present():
    cluster_info = get_kubectl_cluster_info()
    return cluster_info is not None \
        and "kubernetes master" in cluster_info.lower()


def deploy_cluster_master():
    has_cluster = kubernetes_cluster_is_present()
    if has_cluster:
        stdout.info(
            "a cluster is already configured, doing nothing.")
        sys.exit(0)
    stdout.info("deploying a cluster master...")

    stdout.info("(deploying cluster) running kubeadm init...")
    run_cmd("kubeadm init")

    result, output = run_cmd("stat /root/admin.conf")
    if result != 0:
        stdout.info(
            "(copying admin.conf) copying admin.conf for kubeadmuser...")
        run_cmd(
            "cp /etc/kubernetes/admin.conf /root/admin.conf",
            throw_error=True)
        run_cmd(
            "echo 'export KUBECONFIG=/root/admin.conf' " +
            ">> /root/.bashrc",
            throw_error=True)

    stdout.info("(deploying cluster) cluster master creation completed.")


def connect_and_get_join_token():
    result, output = run_cmd("kubeadm token list")
    if result != 0:
        stderr.error("failed to access token list: unexpected exit code 1")
        sys.exit(1)
    output = output.splitlines()
    if not output[0].startswith("TOKEN"):
        stderr.error("failed to access token list: " +
                     "output unexpectedly doesn't start with TOKEN")
        sys.exit(1)
    output = output[1:]
    for line in output:
        if "default bootstrap token" in line:
            token = line.replace("\t", " ").partition(" ")[0].strip()
            print("token: {token}".format(token=token))
            sys.exit(0)
    stderr.error("Failed to get token.")
    sys.exit(1)


def join_cluster(token, ip, port):
    stdout.info('Joining cluster')
    result, output = run_cmd(
        "kubeadm join --token {token} {ip}:{port}".format(
            token=token, ip=ip, port=port))
    if result != 0:
        stderr.error("failed to join: unexpected exit code 1")
        sys.exit(1)
    stdout.info('success.')


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "ssh_arg",
        help="the server to deploy to")
    parser.add_argument(
        "--deploy-cluster-master",
        help="deploy as master of a new cluster",
        dest="cluster_master", default=False,
        action="store_true")
    parser.add_argument(
        "--deploy-cluster-slave",
        help="Join an existing cluster master. You " +
        "also need to specify --token and --master-ip" +
        "when using this option",
        dest="cluster_slave", default=False,
        action="store_true")
    parser.add_argument(
        "--get-join-token",
        help="Query the specified master server for " +
        "joining a slave",
        dest="get_join_token", default=False,
        action="store_true")
    parser.add_argument(
        "--token",
        help="Specify the join token when deploying & joining as a slave. " +
        "If you don't know your join token, use --get-join-token" +
        " on your cluster master.", dest="token")
    parser.add_argument(
        "--master-ip",
        help="Specify the IP address of your master when deploying & " +
             "joining as a slave.", dest="master_ip")
    parser.add_argument(
        "--master-port",
        help="Specify the Port of the master.",
        type=int, default=6443, dest="master_port")
    parser.add_argument(
        "--debug",
        help="Print debug messages",
        dest="debug", default=False,
        action="store_true")
    args = parser.parse_args()
    ssh_arg = shlex.split(args.ssh_arg)
    deploy_as_master = args.cluster_master
    deploy_as_slave = args.cluster_slave
    get_join_token = args.get_join_token
    token = args.token
    master_ip = args.master_ip
    master_port = args.master_port

    if args.debug:
        stderr.setLevel(logging.DEBUG)
        stdout.setLevel(logging.DEBUG)

    # Basic validation of specified tokens:
    if not deploy_as_master and not deploy_as_slave and \
            not get_join_token:
        stderr.error(
            "specify --deploy-cluster-master or " +
            "--deploy-cluster-slave or --get-join-token"
        )
        sys.exit(1)
    if (deploy_as_master + deploy_as_slave + get_join_token) > 1:
        stderr.error(
            "you specified multiple actions.",
        )
        sys.exit(1)
    if deploy_as_slave and (token is None or master_ip is None):
        stderr.error(
            "options --token and --master-ip required " +
            "when using --deploy-cluster-slave",
        )
        sys.exit(1)

    # Perform selected action
    if get_join_token:
        connect_and_get_join_token()
    if deploy_as_master or deploy_as_slave:
        deploy_kubernetes()
    if deploy_as_slave:
        join_cluster(token, master_ip, args.master_port)
    if deploy_as_master:
        deploy_cluster_master()
        connect_and_get_join_token()
